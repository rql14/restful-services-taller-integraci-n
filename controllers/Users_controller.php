<?php
       
class Users_controller extends Controller {

	function __construct() {
		parent::__construct();
	}
        
        /**
         * 
         * @param type $id
         */
        public function getUsers($id = null){
            if(isset($id) && !is_null($id)){
                Penelope::printJSON(Usuario::getById($id));
            }else{
                Penelope::printJSON(Usuario::getAll());
            }
        }
        
        /**
         * 
         */
        public function postUsers(){
            $keys = Usuario::getKeys();
            $this->validateKeys($keys, filter_input_array(INPUT_POST));
            
            $_POST["password"] = Hash::create(filter_input(INPUT_POST, "password"));
            
            $usr = Usuario::instanciate(filter_input_array(INPUT_POST));
            $r = $usr->create();
            
            if($r["error"] == 0){
                $msg = $r["msg"];
                $args = ["user"=>  Penelope::arrayToJSON($usr->toArray())];
            }else{
                $msg = $r["msg"];
                $args = [];
            }
            
            $response = Request::response($msg, $args, $r["error"]);
            Penelope::printJSON($response);
        }
        
        /**
         * 
         */
        public function putUsers(){
            $_PUT = $this->_PUT;
            $usr = Usuario::getById($_PUT["id"]);

            foreach ($_PUT as $key => $value) {
                if($key == "password"){
                    $password = Hash::create(filter_input(INPUT_POST, "password"));
                    $usr->{"set".ucfirst($key)}($password);
                }else{
                    $usr->{"set".ucfirst($key)}($value);
                }
            }
            $r = $usr->update();
            
            if($r["error"] == 0){
                $msg = $r["msg"];
                $args = ["user"=>  Penelope::arrayToJSON($usr->toArray())];
            }else{
                $msg = $r["msg"];
                $args = [];
            }
            
            $response = Request::response($msg, $args, $r["error"]);
            Penelope::printJSON($response);
        }
        
        /**
         * 
         */
        public function deleteUsers(){
            $_DELETE = $this->_DELETE;
            $usr = Usuario::getById($_DELETE["id"]);
            if($usr->getId() != NULL){
                $r = $usr->delete();
                if($r["error"] == 0){
                    $msg = $r["msg"];
                    $args = ["user"=>  Penelope::arrayToJSON($usr->toArray())];
                }else{
                    $msg = $r["msg"];
                    $args = [];
                }
            }else{
                $r["error"]=1;
                $msg = "Invalid User to Delete";
                $args = [];
            }
            $response = Request::response($msg, $args, $r["error"]);
            Penelope::printJSON($response);
            
        }
        public function getSecretKey($username){
            //Request::setHeader(200,"text/plain");
            $usr = Usuario::instanciate(Usuario::getBy("username",$username));
            if(!is_null($usr->getId())){
                $secret = Hash::getSecret($usr);
                $e = 0;
                $msg = "OK";
                $args = ["secret"=>$secret];  
            }else{
               $e = 1;
               $msg = "Not a Valid User";
               $args = [];  
            }
            $response = Request::response($msg, $args, $e);
            Penelope::printJSON($response);
        }
        
        public function getCheckUser($msg,$secret = null){
            //TODO! Add security to Update, POST and DELETE transactions
            //write really checkUser
            Request::setHeader(200,"text/plain");
            $hash = Hash::encrypt($msg, $secret);
            echo "Encrypted data: ".$hash;
            $hash = Hash::decrypt($hash, $secret);
            echo "Decrypted data: ".$hash;
        }
}

spl_autoload_register(function($class) {
    if (file_exists("models/co.edu.usbcali.usuario/" . $class . ".php")) {
        require "models/co.edu.usbcali.usuario/" . $class . ".php";
    }
});
