<?php
/**
 *
 * @author pabhoz
 */
interface IModel {
    public function __construct();
    public function getMyVars();
    public static function getById($id);
}
